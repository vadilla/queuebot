#!/bin/sh

echo "Waiting for database..."

while ! nc -z $MYSQL_HOST $MYSQL_PORT; do
  sleep 2
done

echo "MySQL started!"

python migrate.py db upgrade

exec "$@"