from sqlalchemy.orm import backref

from app import db


class Chat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # chat_id = db.Column(db.Integer)
    active_q = db.Column(db.String(64))
    ques = db.relationship("Que",
                           backref=backref('parent', remote_side=[id]),
                           lazy='dynamic',
                           )


class Que(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer, db.ForeignKey('chat.id'))
    name = db.Column(db.String(64))
    start_time = db.Column(db.DateTime())
    positions = db.relationship("Position",
                                backref=backref('parent', remote_side=[id]),
                                lazy='dynamic',
                                )


class Position(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    q_id = db.Column(db.Integer, db.ForeignKey('que.id'))
    user_id = db.Column(db.Integer)
    user_name = db.Column(db.String(64))
    timestamp = db.Column(db.DateTime())
    __mapper_args__ = {
        "order_by": timestamp
    }
