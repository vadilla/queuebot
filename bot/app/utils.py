def peer_or_user(**kwargs):
    return kwargs['peer_id'] if 'peer_id' in kwargs else kwargs['from_id']
