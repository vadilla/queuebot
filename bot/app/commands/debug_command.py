from app.base_command import BaseCommand


class DebugCommand(BaseCommand):

    def get_keywords(self):
        return ("debug",)

    def execute(self, **kwargs):
        user_id = kwargs['from_id']
        peer_id = kwargs['peer_id'] if 'peer_id' in kwargs else kwargs['from_id']

        answer = ""

        self.vk_api.send_message(peer_id=peer_id, message="Hello, {}".format(self.vk_api.get_name(user_id)))
        return 'ok'
