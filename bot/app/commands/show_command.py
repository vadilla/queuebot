from app import answers
from app import db_api
from app.base_command import BaseCommand


class ShowCommand(BaseCommand):

    def get_keywords(self):
        return ("show",)

    def execute(self, **kwargs):
        peer_id = kwargs['peer_id'] if 'peer_id' in kwargs else kwargs['from_id']
        text = kwargs['text']
        chat = db_api.get_chat(peer_id)

        queue = None
        if text:
            queue = db_api.get_queue(chat, text.strip())
            if not queue:
                self.vk_api.send_message(peer_id, answers.QUEUE_NOT_EXIST.format(text))
                return 'ok'
        elif chat.active_q:
            queue = db_api.get_queue(chat, chat.active_q)
        else:
            self.vk_api.send_message(peer_id, answers.QUEUE_ACTIVE_UNSET)
            return 'ok'

        positions = [str(i.user_name) for i in queue.positions]
        if positions:
            self.vk_api.send_message(peer_id, "\n".join(positions))
        else:
            self.vk_api.send_message(peer_id, answers.QUEUE_EMPTY.format(queue.name))
        return 'ok'
