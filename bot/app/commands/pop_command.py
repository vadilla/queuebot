from app import answers
from app import db_api
from app.base_command import BaseCommand


class PopCommand(BaseCommand):

    def get_keywords(self):
        return ("pop",)

    def execute(self, **kwargs):
        user_id = kwargs['from_id']
        peer_id = kwargs['peer_id'] if 'peer_id' in kwargs else kwargs['from_id']
        text = kwargs['text']
        chat = db_api.get_chat(peer_id)

        queue = None
        if text:
            queue = db_api.get_queue(chat, text.strip())
            if not queue:
                self.vk_api.send_message(peer_id, answers.QUEUE_NOT_EXIST.format(text))
                return 'ok'
        elif chat.active_q:
            queue = db_api.get_queue(chat, chat.active_q)
        else:
            self.vk_api.send_message(peer_id, answers.QUEUE_ACTIVE_UNSET)
            return 'ok'

        if db_api.pop_queue(user_id, queue):
            self.vk_api.send_message(peer_id, answers.QUEUE_POP_SUCCESS)
        else:
            self.vk_api.send_message(peer_id, answers.QUEUE_POP_ERROR)

        return 'ok'
