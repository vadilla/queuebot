from app import answers
from app import db_api
from app.base_command import BaseCommand


class CreateCommand(BaseCommand):

    def get_keywords(self):
        return ("create",)

    def execute(self, **kwargs):
        user_id = kwargs['from_id']
        peer_id = kwargs['peer_id'] if 'peer_id' in kwargs else kwargs['from_id']
        text = kwargs['text'].strip()
        chat = db_api.get_chat(peer_id)

        queue = db_api.create_queue(chat, name=text)

        if queue:
            if not chat.active_q:
                db_api.set_active_queue(chat, queue)
            self.vk_api.send_message(peer_id, answers.QUEUE_CREATE_SUCCESS.format(text))
        else:
            self.vk_api.send_message(peer_id, answers.QUEUE_CREATE_ERROR.format(text))
        return 'ok'
