from app import answers
from app import db_api
from app.base_command import BaseCommand


class TestCommand(BaseCommand):

    def get_keywords(self):
        return ("test",)

    def execute(self, **kwargs):
        user_id = kwargs['from_id']
        peer_id = kwargs['peer_id'] if 'peer_id' in kwargs else kwargs['from_id']
        text = kwargs['text']
        messages = kwargs['fwd_messages']
        res  = self.vk_api.del_messages(peer_id, messages)
        self.vk_api.send_message(peer_id, "kek {}".format(res))
        return 'ok'
