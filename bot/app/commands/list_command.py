from app import answers
from app import db_api
from app.base_command import BaseCommand


class ListCommand(BaseCommand):

    def get_keywords(self):
        return ("list",)

    def execute(self, **kwargs):
        peer_id = kwargs['peer_id'] if 'peer_id' in kwargs else kwargs['from_id']
        chat = db_api.get_chat(peer_id)

        queues = ["{} {}".format(i[0], i[1].name) for i in enumerate(chat.ques)]

        if queues:
            self.vk_api.send_message(peer_id, "\n".join(queues))
        else:
            self.vk_api.send_message(peer_id, answers.NO_QUEUES)

        return 'ok'