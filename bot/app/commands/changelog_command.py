from app import answers
from app.base_command import BaseCommand


class ChangelogCommand(BaseCommand):

    def get_keywords(self):
        return ("changelog",)

    def execute(self, **kwargs):
        user_id = kwargs['from_id']
        peer_id = kwargs['peer_id'] if 'peer_id' in kwargs else kwargs['from_id']

        self.vk_api.send_message(peer_id=peer_id, message=answers.CHANGELOG)
        return 'ok'
