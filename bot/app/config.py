import logging
import os

_basedir = os.path.abspath(os.path.dirname(__file__))

vk_token = os.environ.get('VK_TOKEN')
vk_confirmation_token = os.environ.get('VK_CONFIRMATION_TOKEN')


class AppConfig(object):
    LOG_LEVEL = logging.ERROR  # CRITICAL,ERROR,WARNING,INFO,DEBUG,NOTSET

    MYSQL_USER = str(os.environ.get("MYSQL_USER") or "bot")
    MYSQL_PASSWORD = str(os.environ.get("MYSQL_PASSWORD") or "MemLolSuka123")
    MYSQL_DB = str(os.environ.get("MYSQL_DATABASE") or "qbot")
    MYSQL_HOST = str(os.environ.get("MYSQL_HOST") or "localhost")
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{user}:{password}@{host}/{db}?charset=utf8'.format(
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        host=MYSQL_HOST,
        db=MYSQL_DB,
    )

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_TIMEOUT = 30
    SQLALCHEMY_POOL_RECYCLE = 30
    SQLALCHEMY_MAX_OVERFLOW = 2

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(AppConfig):
    DEBUG = True
    LOG_LEVEL = logging.DEBUG
    SQLALCHEMY_RECORD_QUERIES = True


class ProductionConfig(AppConfig):
    DEBUG = False
    TESTING = False
    LOG_LEVEL = logging.WARNING

    @classmethod
    def init_app(cls, app):
        # multi-step setups could go here
        AppConfig.init_app(app)



config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
