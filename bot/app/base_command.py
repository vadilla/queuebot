from abc import ABC, abstractmethod


class BaseCommand(ABC):

    def __init__(self, vk_api):
        self.vk_api = vk_api

    @abstractmethod
    def get_keywords(self):
        pass

    @abstractmethod
    def execute(self, **kwargs):
        pass
