from flask import request, json

from app import handler
from bot import app


@app.route('/', methods=['POST'])
def index():
    data = json.loads(request.data)
    app.logger.debug(data)
    response = handler.process(data)
    return response
