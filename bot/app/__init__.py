from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy


from app.config import config

db = SQLAlchemy()
migrate = Migrate()
from app import command_handler

handler = command_handler.CommandHandler()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    # with app.app_context():
    #     from app import models
        # db.drop_all()
        # db.create_all()
    return app
