import random

import vk


class VKApi:
    def __init__(self, token):
        self.token = token
        self.session = vk.Session()
        self.api = vk.API(self.session, v=5.103)
        self.MAX_INT = 2 ** 64

    def send_message(self, peer_id, message, attachment=""):
        args = {}
        if peer_id > 2000000000:
            args['chat_id'] = str(peer_id - 2000000000)
        else:
            args['user_id'] = str(peer_id)
        self.api.messages.send(access_token=self.token, message=message, attachment=attachment,
                               random_id=random.randint(0, self.MAX_INT), **args)

    def get_name(self, user_id):
        user = self.api.users.get(user_ids=user_id, access_token=self.token)[0]
        result = user['first_name'] + " " + user['last_name']
        return str(result)
