from app.models import *


def get_chat(chat_id):
    chat = Chat.query.get(chat_id)
    if not chat:
        chat = Chat(id=chat_id)
        db.session.add(chat)
        db.session.commit()
    return chat


def get_queue(chat, name):
    query = chat.ques.filter_by(name=name)
    return query.first() if query.count() > 0 else None


def create_queue(chat, **kwargs):
    if 'name' not in kwargs or get_queue(chat, kwargs['name']):
        return None
    queue = Que(**kwargs)
    chat.ques.append(queue)
    db.session.add(queue)
    db.session.commit()
    return queue


def find_position(queue, user_id):
    query = queue.positions.filter_by(user_id=user_id)
    return query.first() if query.count() > 0 else None


def push_queue(user_id, user_name, timestamp, queue):
    if find_position(queue, user_id):
        return False
    position = Position(user_id=user_id, user_name=user_name, timestamp=timestamp)
    queue.positions.append(position)
    db.session.add(position)
    db.session.commit()
    return True


def pop_queue(user_id, queue):
    position = find_position(queue, user_id)
    if not position:
        return False
    db.session.delete(position)
    db.session.commit()
    return True


def set_active_queue(chat, queue):
    chat.active_q = queue.name
    db.session.commit()


def delete_queue(queue):
    db.session.delete(queue)
    db.session.commit()


def edit_queue():
    pass
