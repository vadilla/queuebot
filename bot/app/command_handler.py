import app.commands.help_command
import app.commands.push_command
import app.commands.create_command
import app.commands.show_command
import app.commands.pop_command
import app.commands.delete_command
import app.commands.list_command
import app.commands.active_command
import app.commands.test_command

from app.config import vk_token, vk_confirmation_token
from app.vk_api import VKApi


class CommandHandler:
    def __init__(self):
        self.links = {}
        vk_api = VKApi(vk_token)
        self.commands = [app.commands.help_command.HelpCommand(vk_api),
                         app.commands.push_command.PushCommand(vk_api),
                         app.commands.create_command.CreateCommand(vk_api),
                         app.commands.show_command.ShowCommand(vk_api),
                         app.commands.pop_command.PopCommand(vk_api),
                         app.commands.active_command.ActiveCommand(vk_api),
                         app.commands.list_command.ListCommand(vk_api),
                         app.commands.delete_command.DeleteCommand(vk_api),
                         app.commands.test_command.TestCommand(vk_api),
                         ]
        for command in self.commands:
            for link in command.get_keywords():
                self.links[link] = command

    # accepts json data from vk
    def process(self, data):
        if 'type' not in data.keys():
            return 'not vk'
        if data['type'] == 'confirmation':
            return vk_confirmation_token
        elif data['type'] == 'message_new':
            message = data['object']['message']
            # if bot was called using @name => split it from the text
            if "club{}".format(data['group_id']) in message['text']:
                message['text'] = message['text'].split(' ', 1)[1]
            # split command word from the text
            splited = message['text'].split(' ', 1)
            command, message['text'] = splited if len(splited) > 1 else (splited[0], None)

            # find function and call command executor
            return self.links[command.lower()].execute(**message) if command.lower() in self.links else 'ok'
